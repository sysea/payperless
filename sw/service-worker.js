importScripts('/js/workbox-sw.js');
const FILES_TO_CACHE = [
	'/index.html', 
	'/css/all.min.css',
	'/css/bootstrap.css',
	'/css/flipclock.min.css',
	'/css/fontawesome.min.css',
	'/css/style.min.css',
	'/js/bootstrap.js',
	'/js/flipclock.min.js',
	'/js/jquery.min.js',
	'/js/jsbarcode.min.js',
	'/js/workbox-sw.js',
	'/js/custom.js'  
];

const CACHE_NAME = 'offline';
const OFFLINE_URL = '/index.html';

self.addEventListener('install', event => {
	event.waitUntil((async () => {
		const cache = await caches.open(CACHE_NAME);
		await cache.add(new Request(OFFLINE_URL, {cache: 'reload'}));
	})());
	//self.skipWaiting();
	caches.open('html-cache').then((cache) => {
		return cache.addAll(FILES_TO_CACHE);
	}, rej => {
		console.log("service worker install event fail: ", rej);
	}); 
}); 

self.addEventListener('activate', function(event) {
	self.clients.claim();
});

self.addEventListener('fetch', (event) => {
	// We only want to call event.respondWith() if this is a navigation request
	// for an HTML page.
	if (event.request.mode === 'navigate') {
	  	event.respondWith((async () => {
			try {

				// First, try to use the navigation preload response if it's supported.
				const preloadResponse = await event.preloadResponse;
				if (preloadResponse) {
					return preloadResponse;
				}
		
				const networkResponse = await fetch(event.request);
				return networkResponse;
				
			} catch (error) {

				// catch is only triggered if an exception is thrown, which is likely
				// due to a network error.
				// If fetch() returns a valid HTTP response with a response code in
				// the 4xx or 5xx range, the catch() will NOT be called.
				console.log('Fetch failed; returning offline page instead.', error);
		
				const cache = await caches.open(CACHE_NAME);
				const cachedResponse = await cache.match(OFFLINE_URL);
				return cachedResponse;

			}
		})());
	}
  
	// If our if() condition is false, then this fetch handler won't intercept the
	// request. If there are any other fetch handlers registered, they will get a
	// chance to call event.respondWith(). If no fetch handlers call
	// event.respondWith(), the request will be handled by the browser as if there
	// were no service worker involvement.
  });

self.addEventListener('push', function(event) {
	var options = {
		body: 'This notification was generated from a push!',
		icon: '/img/icons/icon-144x144.png',
		vibrate: [100, 50, 100],
		data: {
		  dateOfArrival: Date.now(),
		  primaryKey: '2'
		},
		actions: [
		  {action: 'explore', title: 'Explore this new world',
			icon: '/img/icons/icon-144x144.png'},
		  {action: 'close', title: 'Close',
			icon: '/img/logo.png'},
		]
	};
	event.waitUntil(
		self.registration.showNotification('Hello world!', options)
	);
});

self.addEventListener('notificationclick', function(event) {
	event.notification.close();

	event.waitUntil(
		// Do smth here
	);
});

if (workbox) {
	workbox.routing.registerRoute(
		/\.js$/,
		new workbox.strategies.NetworkFirst({
			cacheName: 'html-cache',
		})
	);
	workbox.routing.registerRoute(
		// Cache CSS files.
		/\.css$/,
		// Use cache but update in the background.
		new workbox.strategies.StaleWhileRevalidate({
			// Use a custom cache name.
			cacheName: 'html-cache',
		})
	);
	workbox.routing.registerRoute(
		// Cache image files.
		/\.(?:png|jpg|jpeg|svg|gif)$/,
		// Use the cache if it's available.
		new workbox.strategies.NetworkFirst({
			// Use a custom cache name.
			cacheName: 'image-cache',
			plugins: [
			new workbox.expiration.Plugin({
				// Cache only 20 images.
				maxEntries: 20,
				// Cache for a maximum of a week.
				maxAgeSeconds: 7 * 24 * 60 * 60,
			})
			],
		})
	);
	workbox.routing.registerRoute(
		// Cache CSS files.
		/\.html$/,
		// Use cache but update in the background.
		new workbox.strategies.NetworkFirst({
			// Use a custom cache name.
			cacheName: 'html-cache',
		})
	);

	workbox.routing.registerRoute(
		/\.woff2$/,
		new workbox.strategies.CacheFirst({
			cacheName: 'font-cache'
		})
	);

} else {
	
}	