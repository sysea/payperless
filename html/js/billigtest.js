jQuery(document).ready(function() {
	function prettyPrint() {
		var ugly = document.getElementById('bill').value;
		var obj = JSON.parse(ugly);
		var pretty = JSON.stringify(obj, undefined, 4);
		document.getElementById('bill').value = pretty;
	}

	let email = jQuery('#email');
	let password = jQuery('#password');
	let login = jQuery('#login');
	let testBtn = jQuery('#sendtestbill');
	let bill = jQuery('#bill');
	let ownerId = "";
	let token = "";
	

	login.on('click', function(e) {
		jQuery.post('/api/login', {
			email: email.val(),
			password: password.val()
		}, res => {
			console.log(res);
			token = res.token;
			ownerId = res.ownerId;
		});
	});

	testBtn.on('click', function(e) {
		let billJSON = JSON.parse(bill.val());
		console.log(billJSON);
		if(token == "") {
			alert("Bitte einloggen!");
		} else {
			jQuery.post('/api/tokencheck', {token: token}, res => {
				if(res.valid) {
					billJSON.token = token;
					console.log("Token valid");
					jQuery.post('/api/billings', )
					//jQuery() 
				} else {
					if('error' in res) {
						console.log(res.error);
					}
					alert("Bitte einloggen / erneut einloggen");
				}
			});	
		}
	});
	console.log("ready");
});