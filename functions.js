const fs = require('fs');
const axios = require('axios');
const handlebars = require('handlebars');
const CryptoJS = require('crypto-js');
const mysql = require('mysql2/promise');
const jwtdecoder = require('jwt-decode');
const nodemailer = require('nodemailer');
const deviceDetector = require('device-detector-js');
let deviceDetect = new deviceDetector();

const pool = mysql.createPool({
	host: 'localhost',
	user: process.env.MYSQL_POOL_USER,
	password: process.env.MYSQL_POOL_PASS, 
	database: process.env.MYSQL_POOL_DB,
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0
});

const cardDataPool = mysql.createPool({
	host: 'localhost',
	user: process.env.MYSQL_CARDPOOL_USER,
	password: process.env.MYSQL_CARDPOOL_PASS, 
	database: process.env.MYSQL_CARDPOOL_DB,
	waitForConnections: true,
	connectionLimit: 100,
	queueLimit: 0
});

var methods = {};

methods.resloveReCaptcha = async function(req, res) {
	const secret_key = process.env.SECRET_KEY; //process.env.SECRET_KEY;
	const token = req.body.token;
	const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`;
	let result = false;

	await axios.post(url)
		.then(response => {
			//console.log(console.log(response));
			let google_response = response.data;
			if(google_response.success === true && google_response.score >= 0.5) {
				result = true;
			}
		})
		.catch(error => {
			result = false;
			console.log('114', error);
		});

	return result;
}

methods.checkUserCredentials = async function(data) {
	let correct = false;
	await axios.post('http://ppcms.payper-less.com:1337/auth/local', data)
	.then(response => {
		correct = true;
		return true;
	})
	.catch(reject => {
		correct = false;
		console.log("57", reject);
	});
	return correct;
}

methods.changeCardContactmail = async function(oldMail, newMail) {
	await pool.execute(
		'UPDATE `cards` SET `usercontactmail` = ? WHERE `usercontactmail` = ?',
		[newMail, oldMail]
	);
}

methods.changeUserEmail = async function(oldMail, newMail) {
	let result = false;
	if(oldMail != "" && newMail != "") {
		result = await pool.execute(
			'UPDATE `users-permissions_user` SET `username` = ?, `email` = ? WHERE `email` = ?;',
			[newMail, newMail, oldMail]
		);
	}
	return result;
}

methods.underConstruction = async function(res, req) {
	let page = fs.readFileSync(__dirname + "/html/pages/construction.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Im Aufbau",
		description: "Payperless - der digitale Kassenbon ✓ Jetzt registrieren und mitmachen!"
	};

	let template = handlebars.compile(page);
	page = template({
		header: this.getHeader(tags),
		navigation: this.getNavigation(req),
		menu: this.getSidemenu(req),
		scripts: this.getScripts(),
		lang: "de",
		bodyClass: "construction",
		simpleFooter: this.getPart('tinyfooter')
	});

	res.send(page);
}

methods.loginCardReader = async function() {
	// Login as card reader
	jwtRead = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDREADER_MAIL,
		password: process.env.STRAPI_CARDREADER_PASS,
	})
	.then(response => {
		// Handle success
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Reader Login error');
		return false;
	});
	return jwtRead;
}
 
methods.loginCardWriter = async function() {
	// Login as card updater
	jwtWrite = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDWRITER_MAIL,
		password: process.env.STRAPI_CARDWRITER_PASS,
	})
	.then(response => {
		// Handle success.
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Writer Login error');
		return false;
	});
	return jwtWrite;
}

methods.loginCardConfirmer = async function() {
	// Login as user updater
	jwtUserConfirmer = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDCONFIRMER_MAIL,
		password: process.env.STRAPI_CARDCONFIRMER_PASS
	})
	.then(response => {
		// Handle success.
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Writer Login error');
		return false;
	});
	return jwtUserConfirmer;
}
	
methods.getHeader = function(tags) {
	let html = "";
	html = fs.readFileSync(__dirname + "/html/pages/parts/header.html", "utf-8", function(err, data) {}); 
	let template = handlebars.compile(html);
	let header = template({
		title: tags.title,
		description: tags.description
	});
	return header;
};

methods.getNavigation = function(req) {
	let html = "";
	html = fs.readFileSync(__dirname + "/html/pages/parts/nav.html", "utf-8", function(err, data) {}); 
	let loginaction = "./login";
	if(!this.checkJwtExpiration(req)) {
		loginaction = "./barcode";
	}
	let template  = handlebars.compile(html);
	if(!this.isPhone(req.headers['user-agent'])) {
		if(loginaction == "./barcode") {
			loginaction = "./receipts";
		} 	
	}
	let navigation = template({
		loginaction: loginaction
	});
	return navigation;
}

methods.checkJwtExpiration = function(req) {
	if('session' in req) {
		if('jwt' in req.session) {
			let jwt = jwtdecoder(req.session.jwt);
			let currentTime = new Date().getTime() / 1000;
			if(currentTime > jwt.exp) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	} else {
		return true;
	}
}

methods.resetPassword = async function(email) {
	console.log(email, typeof email);
	axios({
		url: "http://ppcms.payper-less.com:1337/auth/forgot-password",
		method: "post",
		data: {
			email: email,
			url: "https:/payper-less.com/reset"
		}
	})
	.then(resolve => {
		console.log(resolve);
	}).catch(reject => {
		console.log("224", reject.response);
	});
}

methods.getMyUser = async function(jwt) {
	let myUser = false;
	myUser = await axios({
		method: 'get', 
		url: 'http://ppcms.payper-less.com:1337/users/me',
		headers: {
			Authorization: `Bearer ${jwt}`,
		}
	}).then(res => {
		return res.data;
	}).catch(rej => {
		return false;
	});
	return myUser;
}

methods.getCardData = async function(me, jwt) {
	let myCard = false;
	myCard = await axios({
		method: 'get', 
		url: 'http://ppcms.payper-less.com:1337/cards/?user=' + me.id,
		headers: {
			Authorization: `Bearer ${jwt}`,
		}
	}).then(res => {
		return res.data;
	}).catch(rej => {
		return false;
	});
	return myCard;
}
	
methods.getSidemenu = function(req) {
	let html = "";
	let menu = __dirname + "/html/pages/parts/defaultmenu.html";

	if('session' in req && 'cookies' in req) {
		if(req.session.jwt && req.cookies.user_sid) {
			if(!this.checkJwtExpiration(req)) {
				menu = __dirname + "/html/pages/parts/menu.html";
			}
		} 
	}
	let myBarcode = "";
	if(this.isPhone(req.headers['user-agent'])) {
		myBarcode = `
			<li>
				<a href="./barcode" class="pp-nav-link"><i class="fal fa-barcode-alt"></i> Mein Barcode</a>
			</li>
		`;
	}
	html = fs.readFileSync(menu, "utf-8", function(err, data) {}); 

	let template  = handlebars.compile(html);

	let sideMenu = template({
		myBarcode: myBarcode
	});

	return sideMenu;
}; 
	
methods.getScripts = function(scripts = []){
	let html = "";
	if(scripts.length == 0) {
		html = fs.readFileSync(__dirname + "/html/pages/parts/scripts.html", "utf-8", function(err, data) {
			return data;
		}); 
	} else {
		scripts.forEach((value, index) => {
			html += "<script src='js/" + value + ".js'></script>";
		});
	}
	return html;
}

methods.getPart = function(part = "", placeholders = {}) {
	let html = "";
	if(part != "") {
		html = fs.readFileSync(__dirname + "/html/pages/parts/" + part + ".html", "utf-8", function(err, data) {
			return data;
		});
		if(Object.keys(placeholders).length > 0) {
			let template = handlebars.compile(html);
			html = template(placeholders);
		}
	} else {
		console.log("Part not found");
		html = false;
	}
	return html;
}

methods.decryptValue = function(val) {
	let dVal = val;
	let ciphertext = dVal.replace(/ /g, '+'); 

	let bytes = CryptoJS.AES.decrypt(ciphertext.toString(), process.env.CRYPTO_SALT);

	try {
		let plaintext = bytes.toString(CryptoJS.enc.Utf8);
		return plaintext;
	} catch (e) {
		let plaintext = false;
		return plaintext;
	}
}
	
methods.getCardInfosByNumber = async function(cardnumber, token) {
	let answer = await axios.get('http://ppcms.payper-less.com:1337/cards?cardnumber=' + cardnumber, {
		headers: {
			Authorization: `Bearer ${token}`,
		}
	})
	.then(response => {
		let strapiCardObj = response.data[0];
		if(typeof strapiCardObj === 'object' && strapiCardObj !== "undefined" && strapiCardObj !== null) {
			if(Object.keys(strapiCardObj).length > 0) {
				return strapiCardObj;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}).catch(error => {
		console.log("193", error);
		return false;
	}); 

	return answer;
}

methods.checkIfEmailAlreadyUsed = async function(email, token) {
	let anwser = await axios.get('http://ppcms.payper-less.com:1337/cards?usercontactmail=' + email, {
		headers: {
			Authorization: `Bearer ${token}`,
		}
	})
	.then(response => {
		let strapiEmailObj = response.data[0];
		if(typeof strapiEmailObj === 'object' && strapiEmailObj !== "undefined" && strapiEmailObj !== null) {
			if(Object.keys(strapiEmailObj).length > 0) {
				return strapiEmailObj;
			} else {
				return false;
			}
		} else {
			return false;
		}
	})
	.catch(error => {
		return false;
	}); 

	return anwser;
}

methods.sendActivationLink = async function(receiver, cardnumber) {
	let transporter = nodemailer.createTransport({
		host: "smtp.strato.de", 
		port: 465, 
		secure: true,
		auth: {
			user: "info@payper-less.com",
			pass: process.env.PP_INFO_MAIL_PASS
		},
		tls: {
			// do not fail on invalid certs
			rejectUnauthorized: false
		}
	});

	transporter.verify().then((val) => {
			let activationQuery = "m=" + CryptoJS.AES.encrypt(receiver, process.env.CRYPTO_SALT) + "&c=" + CryptoJS.AES.encrypt(cardnumber, process.env.CRYPTO_SALT);

			let message = {
				from: 'info@payper-less.com',
				to: receiver,
				subject: 'Payperless E-Mail-Adresse bestätigen',
				html: `<!doctype html> 
				<html lang='de'>
					<head>
						<meta charset='utf-8'>
						<meta name='viewport' content='width=device-width, initial-scale=1'>
					</head>
					<body>
						<p>Klicken Sie folgende Link um Ihre E-Mail-Adresse für Payperless zu bestätigen</p>
						<a style='font-weight: 700;' href='https://payper-less.com/activate?${activationQuery}'>E-Mail bestätigen</a>
					</body>
				</html>`,
				envelope: {
					from: 'Payperless <info@payper-less.com>',
					to: receiver
				}
			}
			let info = transporter.sendMail(message).then((val) => {
				//console.log(val);
				//console.log(val.messageId);
			}).catch(rej => {
				console.log("rej sendmail", rej);
			});
	}).catch(rej => {
		console.log("281", rej);
	});
}

methods.sendUserActivationLink = async function(id, email) {
	let transporter = nodemailer.createTransport({
		host: "smtp.strato.de", 
		port: 465, 
		secure: true,
		auth: {
			user: "info@payper-less.com",
			pass: process.env.PP_INFO_MAIL_PASS
		},
		tls: {
			// do not fail on invalid certs
			rejectUnauthorized: false
		}
	});

	transporter.verify().then((val) => {
		let activationQuery = "m=" + CryptoJS.AES.encrypt(id.toString(), process.env.CRYPTO_SALT) + "&c=" + CryptoJS.AES.encrypt(email, process.env.CRYPTO_SALT);

		let message = {
			from: 'info@payper-less.com',
			to: email,
			subject: 'Payperless E-Mail-Adresse bestätigen',
			html: `<!doctype html> 
			<html lang='de'>
				<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1'>
				</head>
				<body>
					<p>Klicken Sie folgende Link um Ihre E-Mail-Adresse für Payperless zu bestätigen</p>
					<a style='font-weight: 700;' href='https://payper-less.com/useractivate?${activationQuery}'>E-Mail bestätigen</a>
				</body>
			</html>`,
			envelope: {
				from: 'Payperless <info@payper-less.com>',
				to: email
			}
		}
		transporter.sendMail(message).then((val) => {
			//console.log(val.messageId);
		}).catch(rej => {
			console.log("rej sendmail", rej);
		});
	}).catch(rej => {
		console.log("387", rej);
	});
}

methods.sendContactMail = async function(data, req, res) {

	this.resloveReCaptcha(req, res).then(resolve => {

		if(data.name != "" && data.email != "" && data.message != "") {
			if(resolve) {
	
				let name = data.name;
				let email = data.email;
				let userMessage = data.message;
	
				let transporter = nodemailer.createTransport({
					host: "smtp.strato.de", 
					port: 465, 
					secure: true,
					auth: {
						user: "contact@payper-less.com",
						pass: process.env.PP_CONTACT_MAIL_PASS
					},
					tls: {
						// do not fail on invalid certs
						rejectUnauthorized: false
					}
				});
				
				transporter.verify().then((val) => {				
					let message = {
						from: 'contact@payper-less.com',
						to: 'contact@payper-less.com',
						subject: 'Kontaktanfrage Payperless',
						html: `<!doctype html> 
						<html lang='de'>
							<head>
								<meta charset='utf-8'>
								<meta name='viewport' content='width=device-width, initial-scale=1'>
							</head>
							<body>
								<p>Von: ${name}</p>
								<p>E-Mail: ${email}</p>
								<p>Nachricht: ${userMessage}</p>
							</body>
						</html>`,
						envelope: {
							from: 'Kontakt <contact@payper-less.com>',
							to: 'contact@payper-less.com'
						}
					}
					transporter.sendMail(message).then((val) => {
						res.send({send_success: true});
					}).catch(rej => {
						res.send({send_success: false});
						console.log("rej sendmail", rej);
					});
				}).catch(rej => {
					console.log("281", rej);
				});
	
			} else {
				console.log("recaptcha error");
				res.send({send_success: false});
			}
		} else {
			console.log("data incomplete error");
			res.send({send_success: false});
		}
		
		
	}).catch(reject => {
		console.log("424", reject);
		res.send({send_success: false});
	});

}

methods.getRandomIntInclusive = function(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min +1)) + min; 
} 

methods.generateRandomCustomerID = function() {
	var customerNumbers = [];
	let customID = "";
	for(let i = 0; i < 9; i++) {
		customID += this.getRandomIntInclusive(0, 9).toString(); 
	}
	if(!customerNumbers.includes(customID)) {
		customerNumbers.push(customID);
		return customID;
	} else {
		return false;
	}
}

methods.generateRandomCustomerIDSingle = function() {
	let customID = "";
	for(let i = 0; i < 9; i++) {
		customID += this.getRandomIntInclusive(0, 9).toString(); 
	}
	return customID;
}

methods.concatEan = function() {
	const internals = "250";
	let customID = this.generateRandomCustomerID();
	if(customID) {
		let ean = internals + customID
		return ean;
	} else {
		return false;
	}
}

methods.concatEanSingle = function() {
	const internals = "250";
	let customID = this.generateRandomCustomerIDSingle();
	if(customID) {
		let ean = internals + customID
		return ean;
	} else {
		return false;
	}
}

methods.calculateCheckNumber = function() {
	let numbers = methods.concatEan();
	if(numbers) {
		let sum = 0;
		let counter = 0;
		for(let number of numbers) {
			if(counter % 2 == 0) {
				sum += parseInt(number);
			} else {
				sum += (parseInt(number) * 3);
			}
			counter++;
		}
		let sumString = sum.toString();
		let sumLength = sumString.length - 1;
		let lastNumber = sumString[sumLength];
		let checkNumber = Math.abs((parseInt(lastNumber) - 10));
		if(checkNumber == 10) {
			return false;
		} else {
			numbers += checkNumber.toString();
			return numbers;
		}
	} else {
		return false;
	}
}

methods.calculateCheckNumberSingle = function() {
	let numbers = this.concatEanSingle();
	let sum = 0;
	let counter = 0;
	for(let number of numbers) {
		if(counter % 2 == 0) {
			sum += parseInt(number);
		} else {
			sum += (parseInt(number) * 3);
		}
		counter++;
	}
	let sumString = sum.toString();
	let sumLength = sumString.length - 1;
	let lastNumber = sumString[sumLength];
	let checkNumber = Math.abs((parseInt(lastNumber) - 10));
	if(checkNumber == 10) {
		return "0";
	} else {
		return numbers += checkNumber.toString();
	}
}

methods.calculateCheckNumberExisting = function(cardnumber) {
	let numbers = cardnumber;
	let sum = 0;
	let counter = 0;
	for(let number of numbers) {
		if(counter % 2 == 0) {
			sum += parseInt(number);
		} else {
			sum += (parseInt(number) * 3);
		}
		counter++;
	}
	let sumString = sum.toString();
	let sumLength = sumString.length - 1;
	let lastNumber = sumString[sumLength];
	let checkNumber = Math.abs((parseInt(lastNumber) - 10));
	if(checkNumber == 10) {
		return "0";
	} else {
		return checkNumber.toString();
	}
}

methods.generatePPNumbers = function() {
	var ean = [];
	for(let i = 0; i < 1000; i++) {
		let ppNumber = this.calculateCheckNumber();
		if(ppNumber) {
			ean.push(ppNumber);
		} else {
			i--;
		}
	}
	return ean;
}

async function tryOtherPPNumber() {
	let ppNumber = this.calculateCheckNumberSingle();
	return ppNumber;
}

methods.generatePPNumber = async function(token) {
	let ppNumber = this.calculateCheckNumberSingle();
	let uniqueNumber = false;

	await this.checkIfCardnumberAlreadyExists(ppNumber).then(result => {
		if(result[0][0].count == 0) {
			uniqueNumber = true;
		} else {
			uniqueNumber = false;
		}
	})

	while(!uniqueNumber) {
		await this.checkIfCardnumberAlreadyExists(ppNumber).then(result => {
			if(result[0][0].count == 0) {
				uniqueNumber = true;
			} else {
				ppNumber = tryOtherPPNumber();
			}
		}).catch(reject => {
			uniqueNumber = false;
			console.log("600", reject);
		});
	}
	return ppNumber;
}

methods.checkIfCardnumberAlreadyExists = async function(number) {
	let result = await pool.execute(
		'SELECT COUNT(*) as count FROM cards WHERE cardnumber = ?',
		[number],
		function(err, results, fields) {}
	)
	return result;
}

methods.insertEANs = function() {
	let eans = this.generatePPNumbers();
	let sqlstring = `
		INSERT INTO cards (cardnumber, cardnumberfull)
		VALUES	
	`;
	let counter = 0;
	for(let ean of eans) {
		let eanFull = ean;
		ean = ean.substring(3,12);
		if(counter < eans.length - 1) {
			sqlstring += `('${ean.toString()}', '${eanFull}'),`;
		} else {
			sqlstring += `('${ean.toString()}')`;
		}		 
		counter++; 
	}
	pool.query(sqlstring, (err, results, fields) => {}); 
}

methods.verifyEAN = function(ean) {
	let numbers = ean;
	let sum = 0;
	let counter = 0;
	for(let number of numbers) {
		if(counter % 2 == 0) {
			sum += parseInt(number);
		} else {
			sum += (parseInt(number) * 3);
		}
		counter++;
	}
	if(sum % 10 == 0) {
		return true;
	}     
	return false;
}

methods.isPhone = function(useragent) {
	let agentObj = deviceDetect.parse(useragent);
	if(agentObj.device !== null) {
		if(agentObj.device.type == "smartphone") {
			return true;
		}
	}
	return false;
}

methods.isTablet = function(useragent) {
	let agentObj = deviceDetect.parse(useragent);
	if(agentObj.device !== null) {
		if(agentObj.device.type == "tablet") {
			return true;
		}
	}
	return false;
}

methods.isAndroid = function(useragent) {
	let agentObj = deviceDetect.parse(useragent);
	if(agentObj.os !== null) {
		if(agentObj.os.name == "Android") {
			return true;
		}
	}
	return false;
}

methods.isIOS = function(useragent) {
	let agentObj = deviceDetect.parse(useragent);
	if(agentObj.os !== null) {
		if(agentObj.os.name == "iOS") {
			return true;
		}
	}
	return false;
}

methods.addSupporter = function(req, res) {
	let supporter = req.body;
	let email = supporter.email;
	let notification = supporter.notification;
	let responseObj = {};

	pool.query(
		`INSERT INTO supporter (email, notification) VALUES ('${email}', ${notification})`, 
		(err, results, fields) => {
			if(!err) {
				responseObj.success = true;
				res.send(responseObj);
			} else if(err.errno == 1062) {
				responseObj.already_exists = true;
				res.send(responseObj);
			}
	});	
}

methods.getSupportersCount = async function() {
	var [rows, fields] = await pool.query(`SELECT COUNT(*) AS count FROM supporter`);	
	return await rows[0].count;  
}

methods.getBills = async function(cardnumber, offset = 0) {
	let _offset = offset + 10;
	var [rows, fields] = await cardDataPool.execute(`
		SELECT billinginfo FROM billings WHERE cardnumber = ? LIMIT = 10 OFFSET = ?
	`, [cardnumber, _offset]);
}

methods.getBillsByShop = async function(cardnumber, shop) {

}

methods.getBillsByDate = async function(cardnumber, [from, to]) {

}

methods.getBillsByItem = async function(cardnumber, item) {

}

module.exports = methods;

/* create a svg file of the barcode for offline use */
/*fs.readFile(__dirname + `/html/svg/${responseData.cardnumberFull}.svg`).then(file => {
}).catch(error => {
	let svgNode = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	jsBarcode(svgNode, responseData.cardnumberFull, {
		format: "ean13",
		xmlDocument: document,
		fontSize: 18, 
		textMargin: 0, 
		height: 50, 
		textAlign: "center", 
		marginTop: 25,
		marginRight: 25
	}).blank(20);
	let svgText = xmlSerializer.serializeToString(svgNode);
	fs.outputFile(__dirname + `/html/svg/${responseData.cardnumberFull}.svg`, svgText, err => {}, rej => {
	}).then(file => {}).catch(error => {});
	//console.log("Cr");
});*/
/* ------------------------------------------------- */